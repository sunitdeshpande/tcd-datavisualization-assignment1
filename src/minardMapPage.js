import * as d3 from 'd3';

const DATA_URL_CITIES = './minard-data-cities.csv';
const DATA_URL_TEMP = './minard-data-temp.csv';
const DATA_URL_ARMY = './minard-data-army.csv';

const SELECTOR_MINARD_MAP = '#minardMap';
const SELECTOR_TEMP_PLOT = '#tempPlot';

const PADDING = 50;

class MinardMapPage {

    constructor() {
        // Select charts
        this.minardMap = d3.select(SELECTOR_MINARD_MAP)
            .append('g');
        this.tempPlot = d3.select(SELECTOR_TEMP_PLOT)
            .append('g');

        this.getData()
            .then(() => {
                this.initScale();
                this.plotMinardMap();
                this.plotTemperature();
            });
    }

    getData() {
        const cityDataPromise = d3.csv(DATA_URL_CITIES)
            .then((data) => {
                this.citiesData = data.map((row) => {
                    return {
                        ...row,
                        lat: parseFloat(row.lat),
                        long: parseFloat(row.long)
                    }
                });
            });
        const tempDataPromise = d3.csv(DATA_URL_TEMP)
            .then((data) => {
                this.tempData = data.map((row) => {
                    return {
                        ...row,
                        long: parseFloat(row.long),
                        temp: parseFloat(row.temp)
                    }
                });
            });
        const armyDataPromise = d3.csv(DATA_URL_ARMY)
            .then((data) => {
                this.armyData = data.map((row) => {
                    return {
                        ...row,
                        lat: parseFloat(row.lat),
                        long: parseFloat(row.long)
                    }
                });
            });

        return Promise.all([cityDataPromise, tempDataPromise, armyDataPromise]);
    }

    initScale() {
        const {width, height} = $(SELECTOR_MINARD_MAP).get(0).getBoundingClientRect();
        const cordinates = this.armyData.map((row) => [row.long, row.lat]);
        const features = {
            "type": "FeatureCollection",
            "features": [{
                "type": "Feature",
                "geometry": {
                    "type": "MultiPoint",
                    "coordinates": cordinates
                }
            }]
        };

        this.geoProjection = d3.geoMercator();
        this.geoProjection.fitExtent([[PADDING, PADDING], [width - PADDING, height - PADDING]], features);

        this.lineWidthScale = d3.scaleLinear()
            .domain([0, d3.max(this.armyData.map((row) => row.army_count))])
            .range([1, 30]);

        const tempHeight = $(SELECTOR_TEMP_PLOT).get(0).getBoundingClientRect().height;
        this.temperatureScale = d3.scaleLinear()
            .domain([0, -40])
            .range([PADDING, tempHeight - PADDING]);

    }

    plotMinardMap() {
        this.plotArmyWalk();
        this.plotCities();
        this.addLegend();
        this.addBorderToMinarMap();
    }

    plotCities() {
        // Add circle
        this.minardMap.selectAll('.cities')
            .data(this.citiesData)
            .enter()
            .append('circle')
            .attr('cx', (row) => this.geoProjection([row.long, row.lat])[0])
            .attr('cy', (row) => this.geoProjection([row.long, row.lat])[1])
            .attr('r', 5);

        // Add text
        this.minardMap.selectAll('.cities-label')
            .data(this.citiesData)
            .enter()
            .append('text')
            .attr('x', (row) => this.geoProjection([row.long, row.lat])[0])
            .attr('y', (row) => this.geoProjection([row.long, row.lat])[1] - 10)
            .style('font-size', '0.8em')
            .text((row) => row.name);
    }

    plotArmyWalk() {
        this.minardMap.selectAll('.army-walk')
            .data(this.armyData.slice(0, -1))
            .enter()
            .append('polygon')
            .attr('points', (row, index) => {
                const halfWidth = this.lineWidthScale(row.army_count) / 2;
                const [x1, y1] = this.geoProjection([this.armyData[index].long, this.armyData[index].lat]);
                const [x2, y2] = this.geoProjection([this.armyData[index + 1].long, this.armyData[index + 1].lat]);
                const points = [
                    [x1, y1 - halfWidth],
                    [x2, y2 - halfWidth],
                    [x2, y2 + halfWidth],
                    [x1, y1 + halfWidth],
                    [x1, y1 - halfWidth]
                ];
                return points.map((row) => row.join(',')).join(' ');
            })
            .style("fill", (row) => {
                if (row.direction === 'R')
                    return 'rgba(0,0,0, 0.6)';
                switch (row.group) {
                    case '1':
                        return 'rgb(230, 205, 171)';
                    case '2':
                        return 'rgb(50, 50, 255)';
                    case '3':
                        return 'rgb(0, 188, 0)';
                }
            })
            .style('stroke', 'black')
            .style('stroke-width', 0.1);

        const labelPath = d3.line()
            .curve(d3.curveBasis)
            .x((row) => {
                return this.geoProjection([row.long, row.lat])[0]
            })
            .y((row) => this.geoProjection([row.long, row.lat])[1]);

        this.minardMap.selectAll('.army-walk-label-path')
            .data(this.armyData.slice(0, -1))
            .enter()
            .append('path')
            .attr('id', (_, index) => `walk-label-path-${index}`)
            .attr('d', (_, index) => labelPath([this.armyData[index], this.armyData[index + 1]]))
            .style('stroke', 'none');

        this.minardMap.selectAll('.army-walk-label')
            .data(this.armyData.slice(0, -1))
            .enter()
            .append('text')
            .style('font-size', '0.5em')
            .attr('dy', '0.7em')
            .attr('x', (_, index) => {
                const [x1, y1] = this.geoProjection([this.armyData[index].long, this.armyData[index].lat]);
                const [x2, y2] = this.geoProjection([this.armyData[index + 1].long, this.armyData[index + 1].lat]);
                return Math.abs((x2 - x1) / 2);
            })
            .append('textPath')
            .attr('xlink:href', (_, index) => `#walk-label-path-${index}`)
            .text((row) => row.army_count);
    }

    addBorderToMinarMap(){
        const {width, height} = $(SELECTOR_MINARD_MAP).get(0).getBoundingClientRect();
        this.minardMap.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", width)
            .attr("height", height)
            .style("stroke", 'black')
            .style("fill", "none")
            .style("stroke-width", 0.5);
    }

    addLegend(){
        const legend = [
            ['Group 1', 'rgb(230, 205, 171)'],
            ['Group 2', 'rgb(50, 50, 255)'],
            ['Group 3', 'rgb(0, 188, 0)'],
            ['Return Journey', 'rgba(0,0,0, 0.7)'],
        ];
        const {width, height} = $(SELECTOR_MINARD_MAP).get(0).getBoundingClientRect();

        this.minardMap.selectAll('.legend-circle')
            .data(legend)
            .enter()
            .append('circle')
            .attr('cx', width - 200)
            .attr('cy', (_, index) => height - 15 - (index * 25))
            .attr('r', 10)
            .style('fill', (row) => row[1])
            .style('stroke', 'black');

        this.minardMap.selectAll('.legend-label')
            .data(legend)
            .enter()
            .append('text')
            .attr('x', width - 180)
            .attr('y', (_, index) => height - 8 - (index * 25))
            .text((row)=> `- ${row[0]}`);
    }

    plotTemperature(){
        const { width } = $(SELECTOR_TEMP_PLOT).get(0).getBoundingClientRect();
        this.tempPlot.selectAll('.tempLine')
            .data(this.tempData.slice(0, -1))
            .enter()
            .append('line')
            .attr('x1', (_, index) => this.geoProjection([this.tempData[index].long, 50])[0])
            .attr('y1', (_, index) => this.temperatureScale(this.tempData[index].temp))
            .attr('x2', (_, index) => this.geoProjection([this.tempData[index + 1].long, 50])[0])
            .attr('y2', (_, index) => this.temperatureScale(this.tempData[index + 1].temp))
            .style('stroke', 'red')
            .style('stroke-width', 2);

        this.tempPlot.selectAll('.tempLabel')
            .data(this.tempData)
            .enter()
            .append('text')
            .attr('x', (row) => this.geoProjection([row.long, 50])[0] - 15)
            .attr('y', (row) => this.temperatureScale(row.temp) - 15)
            .style('font-size', '0.8em')
            .style('font-weight', 'bold')
            .text((row) => {
                if(row.day && row.month){
                   return `${row.day}, ${row.month}`;
                }
                return '';
            });

        const axis = d3.axisRight()
            .scale(this.temperatureScale);

        this.tempPlot.append('g')
            .attr("transform", "translate(" + (width - 50) + " ,0)")
            .call(axis);

        this.tempPlot.selectAll('.grid-lines')
            .append('g')
            .data(d3.range(5))
            .enter()
            .append('line')
            .attr('x1', 0)
            .attr('y1', (value) => this.temperatureScale(value * -10))
            .attr('x2', width - PADDING)
            .attr('y2', (value) => this.temperatureScale(value * -10))
            .style('stroke', 'black')
            .style('stroke-width', 0.5)
            .style('opacity', 0.5);
    }
}


new MinardMapPage();