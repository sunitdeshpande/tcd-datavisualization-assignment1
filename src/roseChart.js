import * as d3 from 'd3';

export default class RoseChart {

    constructor(config) {
        this.config = config;

        // Init scales
        this.angleScale = this.getAngleScaleFromConfig();
        this.radiusScale = this.getRadiusScale();

        // Arc functions
        this.arcFunction = this.getArcFunction();
        this.labelArcFunction = this.getLabelArcFunction();

        this.container = d3.select(config.selector)
            .append('g');
        this.applyContainerAttributes();
    }

    draw() {
        // Loop data and draw arc
        this.config.data.forEach((row, rowIndex) => {
            // Draw arc
            const arcContainer = this.container.append('g')
                .selectAll('.arc')
                .data(Object.entries(row))
                .enter()
                .append('path')
                .attr('d', (rowEntry) => {
                    return this.arcFunction({
                        index: rowIndex,
                        value: rowEntry[1],
                        style: this.config.styles[rowEntry[0]]
                    })
                })
                .style('stroke', (entry) => this.config.styles[entry[0]].stroke || 'black')
                .style('stroke-width', (entry) => this.config.styles[entry[0]].strokeWidth || 1)
                .style('fill', (entry) => this.config.styles[entry[0]].fill || 'none');

            // Add hover title
            arcContainer.append('title')
                .text((rowEntry) => {
                    const value = rowEntry[1];
                    const titleFunction = this.config.styles[rowEntry[0]].title;
                    if (titleFunction) {
                        return titleFunction(value);
                    }
                    return value;
                })
        });

        // Draw label
        this.container.append('g')
            .selectAll('.arc-label')
            .data(this.config.data)
            .enter()
            .append('path')
            .attr('id', (_, index) => this.config.selector + 'labelArc' + index)
            .attr('d', this.labelArcFunction)
            .style('fill', 'none')
            .style('stroke', 'none');

        this.container.selectAll('.arcLabel')
            .data(this.config.data)
            .enter()
            .append('text')
            .attr('class', (_, index) => this.config.selector.replace('#', '') + '-labelArc')
            .attr('x', 20)
            .attr('dy', '-.71em')
            .append('textPath')
            .attr('xlink:href', (_, index) => '#' + this.config.selector + 'labelArc' + index)
            .text((_, index) => this.config.labels[index]);
    }

    rotate(value) {
        this.config.rotate = value;
        this.applyContainerAttributes();
    }

    scale(value) {
        this.config.scale = value;
        this.applyContainerAttributes();
    }

    getX() {
        return this.config.x;
    }

    setX(value) {
        this.config.x = value;
        this.applyContainerAttributes();
    }

    getY() {
        return this.config.y;
    }

    setY(value) {
        this.config.y = value;
        this.applyContainerAttributes();
    }

    getAngleScaleFromConfig() {
        return d3.scaleLinear()
            .range([0, Math.PI * 2])
            .domain([0, this.config.data.length]);
    }

    getRadiusScale() {
        return d3.scaleLinear()
            .range([0, d3.min([this.config.width, this.config.height])])
            .domain([0, this.getRadiusForValue(this.getMaxForData(this.config.data))]);
    }

    getArcFunction() {
        return d3.arc()
            .innerRadius(0)
            .outerRadius(row => this.radiusScale(this.getRadiusForValue(row.value)))
            .startAngle(row => this.angleScale(row.index))
            .endAngle(row => this.angleScale(row.index + 1));
    }

    getLabelArcFunction() {
        return d3.arc()
            .innerRadius(0)
            .outerRadius(row => {
                const maxValue = d3.max([100, row.diseases, row.wounds, row.others]);
                return this.radiusScale(this.getRadiusForValue(maxValue));
            })
            .startAngle((_, index) => this.angleScale(index))
            .endAngle((_, index) => this.angleScale(index + 1));
    }

    getRadiusForValue(value) {
        return Math.sqrt((2 * value) / this.angleScale(1));
    }

    getMaxForData(data) {
        const flattenArray = [].concat.apply([], data.map(function (row) {
            return [row.diseases, row.wounds, row.others]
        }));
        return d3.max(flattenArray);
    }

    applyContainerAttributes() {
        this.container
            .attr('width', this.config.width || 100)
            .attr('height', this.config.height || 100)
            .attr('transform', this.getContainerAttributeString(this.config));
    }

    animateAttributes(newConfig, duration){
       this.container
           .transition()
           .duration(duration)
           .attrTween('transform', () => d3.interpolateString( this.getContainerAttributeString(this.config), this.getContainerAttributeString(newConfig)));
    }

    getContainerAttributeString(config){
        return 'translate(' + (config.x || 0) + ',' + (config.y || 0) + ')'
            + 'rotate(' + (config.rotate || 0) + ')'
            + 'scale(' + (config.scale || 1) + ')'
    }
};
