import * as d3 from 'd3';

import RoseChart from './roseChart';

const DATA_URL = './rose-chart-data.csv';

class RoseChartPage {

    constructor() {

        // Download data and draw chart
        d3.csv(DATA_URL)
            .then((response) => {
                let chartsData = this.spiltData(response);

                const chart1Labels = this.getLabelsFromData(chartsData[0]);
                const chart1Data = this.formatData(chartsData[0]);

                const chart2Labels = this.getLabelsFromData(chartsData[1]);
                const chart2Data = this.formatData(chartsData[1]);

                this.chart1 = this.getChartFromData('#chart1', chart1Data, chart1Labels, 280, 200, 250, 250);
                this.chart2 = this.getChartFromData('#chart2', chart2Data, chart2Labels, 220, 350, 400, 300);
                this.staticChart = this.getChartFromData('#staticChart', chart1Data, chart1Labels, 280, 350, 250, 250);

                this.chart1.draw();
                this.chart2.draw();
                this.staticChart.draw();
                this.animatedStaticChart();
            })
            .catch(function (error) {
                alert("Error: Couldn't download data");
                console.log(error);
            });

        this.initSliders();
        this.initEventHandler();
    }

    spiltData(data) {
        return [data.filter((_, index) => index < 12), data.filter((_, index) => index >= 12)];
    }

    getLabelsFromData(data) {
        return data.map((row) => row.month);
    }

    formatData(data) {
        return data.map((row) => ({
            diseases: parseFloat(row.diseases),
            wounds: parseFloat(row.wounds),
            others: parseFloat(row.others)
        }));
    }

    getChartFromData(selector, data, labels, x, y, width, height) {
        return new RoseChart({
            selector: selector,
            x: x,
            y: y,
            width: width,
            height: height,
            data: data,
            labels: labels,
            styles: {
                diseases: {
                    fill: 'rgba(0, 172, 193, 0.85)',
                    title: (data) => 'Diseases: ' + data + ' per thousand'
                },
                wounds: {
                    fill: 'rgba(109, 76, 65, 0.85)',
                    title: (data) => 'Others: ' + data + ' per thousand'
                },
                others: {
                    fill: 'rgba(216, 27, 96, 0.85)',
                    title: (data) => 'Wounds: ' + data + ' per thousand'
                }
            }
        });
    }

    initSliders() {
        this.rotateSlider = $('#sliderRotate')
            .slider({
                formatter: (value) => 'Current value: ' + value
            })
            .on('change', this.onRotateValueChanged.bind(this))
            .data('slider');

        this.scaleSlider = $('#sliderScale')
            .slider({
                formatter: (value) => 'Current value: ' + value
            })
            .on('change', this.onScaleValueChanged.bind(this))
            .data('slider');

    }

    initEventHandler() {
        $('#btnLeft').on('click', () => {
            this.chart1.setX(this.chart1.getX() - 5);
            this.chart2.setX(this.chart2.getX() - 5);
        });

        $('#btnRight').on('click', () => {
            this.chart1.setX(this.chart1.getX() + 5);
            this.chart2.setX(this.chart2.getX() + 5);
        });

        $('#btnUp').on('click', () => {
            this.chart1.setY(this.chart1.getY() - 5);
            this.chart2.setY(this.chart2.getY() - 5);
        });

        $('#btnDown').on('click', () => {
            this.chart1.setY(this.chart1.getY() + 5);
            this.chart2.setY(this.chart2.getY() + 5);
        });

        $('#btnAnimate').on('click', this.animatedStaticChart.bind(this));
    }

    onRotateValueChanged() {
        const value = this.rotateSlider.getValue();
        this.chart1.rotate(value);
        this.chart2.rotate(value);
    }

    onScaleValueChanged() {
        const value = this.scaleSlider.getValue();
        this.chart1.scale(value);
        this.chart2.scale(value);
    }

    animatedStaticChart(){
        this.staticChart.animateAttributes({
            x: this.staticChart.getX() + 500,
            y: this.staticChart.getY(),
            rotate: 360,
            scale: 1.5
        }, 3000)
    }
}

new RoseChartPage();

