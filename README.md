# Data Visulization - CS7DS4
<p style="text-align: center">By: <i>Sunit Deshpande</i></p>

Data visualization for Rose Chart and Sankey diagram of Napoleon's loss during the Russian campaign of 1812.

Project is live on the following url [https://sunitdeshpande.gitlab.io/tcd-datavisualization-assignment1/](https://sunitdeshpande.gitlab.io/tcd-datavisualization-assignment1/).

# Installation

Following are step for installation of the project.

## Prerequisite

Nodejs and npm is required for the project.

## Steps

1. Clone the Project

2. Install the required libraries to run the project.

    Go to the project directory and run the following command.

    ```shell script
    npm install
    ```
3. To Run the project locally run the following command

    ```shell script
   npm run dev
    ``` 
4. Open a web browser (Chrome Preferred) and go the following url [http://localhost:1234/](http://localhost:1234/)


